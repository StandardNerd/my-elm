--
-- 1. Using function composition, create a function named `wordCount` that returns the number of words in a sentence.
-- 2. Call the `wordCount` function with any sentence and display the result on the page.


module Exercise1_2 exposing (main)

import Html


wordCount str =
    String.split " " str |> List.length


main =
    let
        string1 =
            "Foo foobar bar baz"
    in
    wordCount string1
        |> String.fromInt
        |> Html.text
