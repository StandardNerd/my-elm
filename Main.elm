module Main exposing (add, main, result)

import Html exposing (..)
import String


add a b =
    a + b



-- Partial application
-- add : a -> b -> a + b
-- add3 = add 3
-- add3 : b -> 3 + b
--  result = add3 1
-- Forward Pipe Operator
-- add 1 2 |> add 3
-- add 1 2 |> (b -> 3 + b)


result =
    -- add (add 1 2) 3
    -- add 1 2 |> add 3
    add 1 2 |> (\a -> remainderBy 2 a == 0)


stringFromBool bool =
    if bool then
        "true"

    else
        "false"


main =
    Html.text (stringFromBool result)
