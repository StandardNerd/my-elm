-- 1. Call your new function `~=` as a prefix function.
-- 2. Display the result on the page


module Exercise1_2 exposing (main)

import Html


checkForEqualityOfFirstLetter str1 str2 =
    String.left 1 str1 == String.left 1 str2


stringFromBool bool =
    if bool then
        "true"

    else
        "false"


main =
    let
        string1 =
            "Foo"

        string2 =
            "Far"
    in
    "string 1: "
        ++ string1
        ++ " string 2: "
        ++ string2
        ++ " erstes Zeichen ist gleich? "
        ++ stringFromBool (checkForEqualityOfFirstLetter string1 string2)
        |> Html.text
