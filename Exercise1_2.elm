-- 1. Create a new file named `Exercise1.elm`
-- 2. Install the Html module
-- 3. Import the Html Module into the `Exercise1.elm` file
-- 3. Show your name on the page


module Exercise1_2 exposing (main)

import Html


uppercasesString maxLength str =
    if String.length str > maxLength then
        String.toUpper str

    else
        str


main =
    let
        name =
            "asdfad asdf"

        length =
            String.length name
    in
    Html.text (String.concat [ uppercasesString 10 name, " name length: ", String.fromInt length ])
